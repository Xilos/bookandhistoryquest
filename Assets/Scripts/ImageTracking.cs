﻿using System.Linq;

using UnityEngine;
using UnityEngine.XR.ARFoundation;


public class ImageTracking : MonoBehaviour
{
	private ScenePicker _scenePicker;

	private ARTrackedImageManager _manager;

	public GameObject spawningObject;


	private void Awake()
	{
		_manager = FindObjectOfType<ARTrackedImageManager>();
		_scenePicker = FindObjectOfType<ScenePicker>();

		spawningObject.SetActive(false);
	}

	private void OnEnable()
	{
		_manager.trackedImagesChanged += ManagerOntrackedImagesChanged;
	}

	private void OnDisable()
	{
		_manager.trackedImagesChanged -= ManagerOntrackedImagesChanged;
	}

	private void ManagerOntrackedImagesChanged(ARTrackedImagesChangedEventArgs args)
	{
		if (args.added.Any() || args.updated.Any())
		{
			ARTrackedImage image = args.added.FirstOrDefault() ?? args.updated.First();
			UpdateImage(image);
		}

		if (args.removed.Any())
		{
			spawningObject.SetActive(false);
		}
	}

	private void UpdateImage(Component image)
	{
		Vector3 position    = image.transform.position;
		Quaternion rotation = image.transform.rotation;
		Vector3 scale       = image.transform.localScale;

		spawningObject.transform.position   = position;
		spawningObject.transform.rotation   = rotation;
		spawningObject.transform.localScale = scale;

		spawningObject.SetActive(true);
	}
}
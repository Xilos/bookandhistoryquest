﻿using UnityEngine;

public class SimpleAnswerController : MonoBehaviour
{
	public GameObject wrongAnswerMessage;

	public void OpenWrongAnswerMessage() => wrongAnswerMessage.SetActive(true);

	public void CloseWrongAnswerMessage() => wrongAnswerMessage.SetActive(false);
}

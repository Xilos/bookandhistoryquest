﻿using UnityEngine;
using UnityEngine.UI;


public class ToggleElement : MonoBehaviour
{
	public bool isRight;

	[SerializeField]
	private Text _toggleText;

	private bool _isChecked;


	public bool IsRight => isRight;

	public bool IsChecked => _isChecked;

	public string Name => _toggleText.text;


	public void OnValueChanged(bool value) => _isChecked = value;
}

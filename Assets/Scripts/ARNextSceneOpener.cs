﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;


public class ARNextSceneOpener : MonoBehaviour
{
	private ARTrackedImageManager _manager;


	private void Awake()
	{
		_manager = FindObjectOfType<ARTrackedImageManager>();
		_manager.trackedImagesChanged += ManagerOntrackedImagesChanged;
	}

	private void OnEnable()
	{
		//_manager.trackedImagesChanged += ManagerOntrackedImagesChanged;
	}

	private void OnDisable()
	{
		_manager.trackedImagesChanged -= ManagerOntrackedImagesChanged;
	}

	private static void ManagerOntrackedImagesChanged(ARTrackedImagesChangedEventArgs obj)
	{
		ScenePicker.OpenNextSceneStatic();
	}
}

﻿using UnityEngine;

public class GameRay : MonoBehaviour
{
	private RaycastHit _hit;

	public GameObject HitObject { get; set; }

	public static GameRay Instance { get; set; }

	private void Awake() => Instance = this;

	// Update is called once per frame
	private void Update()
	{
		if (Camera.current == null)
		{
			return;
		}

		Ray ray;
		if (Input.touchSupported)
			ray = Camera.current.ScreenPointToRay(Input.GetTouch(0).position);
		else
			ray = Camera.current.ScreenPointToRay(Input.mousePosition);

		bool touchedWithOneFinger   = Input.touchSupported            &&
		                              Input.GetTouch(0).tapCount == 1 &&
		                              Input.GetTouch(0).phase == TouchPhase.Ended;

		bool leftMouseButtonClicked = Input.GetMouseButtonDown(0);

		if (Physics.Raycast(ray, out _hit) && (leftMouseButtonClicked || touchedWithOneFinger))
		{
			HitObject = _hit.transform.gameObject;
		}
		else
		{
			HitObject = null;
		}
	}
}
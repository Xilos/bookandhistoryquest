﻿using UnityEngine;
using UnityEngine.UI;


public class UppercaseIt : MonoBehaviour
{
	private InputField _input;

	private void Awake()
	{
		_input = GetComponent<InputField>();
	}

	public void OnInputChanged(string str)
	{
		_input.text = _input.text.ToUpper();
	}
}

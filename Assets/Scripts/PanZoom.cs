﻿using System;

using UnityEngine;
using UnityEngine.UI;


public class PanZoom : MonoBehaviour
{
	private float width;
	private float height;

	private Image _image;

	public float zoomOutMin = 1;
	public float zoomOutMax = 8;


	void Awake()
	{
		_image = GetComponent<Image>();

		if (_image == null)
		{
			Destroy(this);
			return;
		}

		width  = _image.rectTransform.rect.width;
		height = _image.rectTransform.rect.height;
	}

	void Update()
	{
		switch (Input.touchCount)
		{
			case 2:
			{
				Touch touchZero = Input.GetTouch(0);
				Touch touchOne  = Input.GetTouch(1);

				Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
				Vector2 touchOnePrevPos  = touchOne.position  - touchOne.deltaPosition;

				float prevMagnitude    = (touchZeroPrevPos   - touchOnePrevPos)   .magnitude;
				float currentMagnitude = (touchZero.position - touchOne.position) .magnitude;

				float difference = currentMagnitude - prevMagnitude;

				Zoom(difference * 0.01f);
				break;
			}
			case 1:
			{
				float scale = _image.transform.localScale.x;
				
				Touch touch = Input.GetTouch(0);
				Vector2 touchDeltaPosition = touch.deltaPosition;
				var delta = new Vector3(touchDeltaPosition.x, touchDeltaPosition.y, 0);

				_image.transform.position += delta;

				break;
			}
			case 0:
				CorrectImagePosition();

				break;
		}
	}


	private void Zoom(float increment)
	{
		float oldScale = _image.transform.localScale.x;
		float newScaleValue = Mathf.Clamp(oldScale + increment, zoomOutMin, zoomOutMax);
		_image.transform.localScale = new Vector3(newScaleValue, newScaleValue, newScaleValue);
	}

	private void CorrectImagePosition()
	{
		var scale    = _image.transform.localScale.x;
		var position = _image.transform.localPosition;

		var xMax = width  / 2 * (scale - 1);
		var yMax = height / 2 * (scale - 1);

		if (Math.Abs(position.x) > xMax)
		{
			position.x = xMax * Math.Sign(position.x);
		}

		if (Math.Abs(position.y) > yMax)
		{
			position.y = yMax * Math.Sign(position.y);
		}

		_image.transform.localPosition = position;
	}
}

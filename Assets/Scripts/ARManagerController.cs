﻿using System.Linq;

using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;


public class ARManagerController : MonoBehaviour
{
	private ARTrackedImageManager _manager;


	private void Awake()
	{
		_manager = FindObjectOfType<ARTrackedImageManager>();

		//_manager.trackedImagePrefab.SetActive(false);
	}

	private void OnEnable()
	{
		_manager.trackedImagesChanged += ManagerOntrackedImagesChanged;
	}

	private void OnDisable()
	{
		_manager.trackedImagesChanged -= ManagerOntrackedImagesChanged;
	}

	private void ManagerOntrackedImagesChanged(ARTrackedImagesChangedEventArgs args)
	{
		if (args.removed.Any())
		{
			_manager.trackedImagePrefab.SetActive(false);
			return;
		}

		ARTrackedImage image = args.added.FirstOrDefault() ?? args.updated.First();
		//GameObject prefab = image.transform.GetChild(0).gameObject;

		image.gameObject.SetActive(image.trackingState == TrackingState.Tracking);
	}
}

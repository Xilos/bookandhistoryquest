﻿using System.ComponentModel;

using UnityEngine;
using UnityEngine.SceneManagement;


[System.Serializable]
public class ScenePicker : MonoBehaviour
{
	private const string PreviousLevelKey = "previousLevel";

	[SerializeField]
	[Description("Предыдущая сцена")]
	public string prevScene;

	[SerializeField]
	[Description("Следующая сцена")]
	public string nextScene;

	[SerializeField]
	[Description("Сцена со справкой")]
	public string helpScene;

	private static string _prevScene;
	private static string _nextScene;
	private static string _helpScene;


	void Awake()
	{
		SceneManager.sceneUnloaded += SceneManager_OnsceneUnloaded;
	}

	private void Start()
	{
		_prevScene = prevScene;
		_nextScene = nextScene;
		_helpScene = helpScene;
	}

	public static void OpenPrevSceneStatic() => SceneManager.LoadScene(_prevScene);

	public static void OpenNextSceneStatic() => SceneManager.LoadScene(_nextScene);

	public static void OpenHelpSceneStatic() => SceneManager.LoadScene(_helpScene);

	public void OpenPrevScene() => SceneManager.LoadScene(prevScene);

	public void OpenNextScene() => SceneManager.LoadScene(nextScene);

	public void OpenHelpScene() => SceneManager.LoadScene(helpScene);

	public void GoBackScene()
	{
		int previousLevel = PlayerPrefs.GetInt(PreviousLevelKey);
		SceneManager.LoadScene(previousLevel);
	}


	private void SceneManager_OnsceneUnloaded(Scene scene)
	{
		PlayerPrefs.SetInt(PreviousLevelKey, scene.buildIndex);

		SceneManager.sceneUnloaded -= SceneManager_OnsceneUnloaded;
	}
}
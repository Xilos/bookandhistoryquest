﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;


public class TrackedImageInstatiater : MonoBehaviour
{
	[SerializeField]
	private Texture marker;

	[SerializeField]
	private GameObject arPrefab;

	private ARTrackedImageManager m_TrackedImageManager;

	private void Awake()
	{
		m_TrackedImageManager = GetComponent<ARTrackedImageManager>();
	}

	private void OnEnable()
	{
		m_TrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
	}

	private void OnDisable()
	{
		m_TrackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
	}

	private void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
	{
		foreach (ARTrackedImage trackedImage in eventArgs.added)
		{
			if (trackedImage.referenceImage.name == marker.name)
			{
				Instantiate(arPrefab);
			}
		}

		foreach (ARTrackedImage trackedImage in eventArgs.updated)
		{
			if (trackedImage.referenceImage.name == marker.name)
			{
				Instantiate(arPrefab);
			}
		}
	}
}

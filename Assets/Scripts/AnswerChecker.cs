﻿using System;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;


public class AnswerChecker : MonoBehaviour
{
	private Color _wrongAnswerColor = new Color(0.925f, 0.31f, 0.31f, 0.3f);

	private Color _initialInputFieldColor;


	public ScenePicker scenePicker;

	public InputField[] inputFields;

	public string[] rightAnswers;


	public void Start()
	{
		_initialInputFieldColor = inputFields.First().colors.normalColor;
	}

	public void OpenNextScene()
	{
		foreach (var inputField in inputFields)
		{
			string userAnswer = inputField.text;
			if (rightAnswers.Any(answer => string.Equals(userAnswer, answer, StringComparison.InvariantCultureIgnoreCase)))
				continue;

			SetInputFieldColor(inputField, _wrongAnswerColor);
			AnimationScript.ShowWrongAnswerAnimation();

			return;
		}

		ScenePicker.OpenNextSceneStatic();
	}

	public void OnTextChanged(InputField inputField) => SetInputFieldColor(inputField, _initialInputFieldColor);


	private void SetInputFieldColor(InputField inputField, Color color)
	{
		ColorBlock cb = inputField.colors;
		cb.normalColor = color;
		cb.highlightedColor = color;
		cb.selectedColor = color;
		inputField.colors = cb;
	}
}

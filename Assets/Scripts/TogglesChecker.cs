﻿using UnityEngine;


public class TogglesChecker : MonoBehaviour
{
	public ToggleElement[] _toggles;

	public ScenePicker scenePicker;


	public void OpenNextScene()
	{
		bool canOpenNextScene = true;
		string wrongCheckedToggles = null;

		foreach (ToggleElement toggle in _toggles)
		{
			if (toggle.IsRight)
			{
				if (!toggle.IsChecked)
				{
					canOpenNextScene = false;
				}
			}
			else
			{
				if (toggle.IsChecked)
				{
					wrongCheckedToggles += string.IsNullOrEmpty(wrongCheckedToggles) ? toggle.Name : $", {toggle.Name}";
					canOpenNextScene = false;
				}
			}
		}

		if (canOpenNextScene)
		{
			scenePicker.OpenNextScene();
			return;
		}

		if (string.IsNullOrEmpty(wrongCheckedToggles))
		{
			scenePicker.OpenHelpScene();
			return;
		}

		PlayerPrefs.SetString("WrongCheckedToggles", wrongCheckedToggles);
		scenePicker.OpenPrevScene();
	}
}

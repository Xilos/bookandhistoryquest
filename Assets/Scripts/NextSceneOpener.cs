﻿using UnityEngine;

public class NextSceneOpener : MonoBehaviour
{
	private void Awake()
	{
		ScenePicker.OpenNextSceneStatic();
		Destroy(this);
	}
}

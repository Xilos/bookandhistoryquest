using TMPro;

using UnityEngine;
using UnityEngine.EventSystems;


[RequireComponent(typeof(TextMeshProUGUI))]
public class OpenHyperlinks : MonoBehaviour, IPointerClickHandler
{
	public void OnPointerClick(PointerEventData eventData)
	{
		var tmpText = GetComponent<TMP_Text>();
		int linkIndex = TMP_TextUtilities.FindIntersectingLink(tmpText, Input.mousePosition, Camera.current);
		if (linkIndex != -1)
		{
			// was a link clicked?
			TMP_LinkInfo linkInfo = tmpText.textInfo.linkInfo[linkIndex];

			// open the link id as a url, which is the metadata we added in the text field
			Application.OpenURL(linkInfo.GetLinkID());
		}
	}
}
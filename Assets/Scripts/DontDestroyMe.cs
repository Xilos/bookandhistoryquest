﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class DontDestroyMe : MonoBehaviour
{
	private void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}

	private void Start()
	{
		SceneManager.sceneLoaded += SceneManager_OnsceneLoaded;
	}

	private void SceneManager_OnsceneLoaded(Scene scene, LoadSceneMode mode)
	{
		if (scene.buildIndex == 0)
		{
			SceneManager.sceneLoaded -= SceneManager_OnsceneLoaded;
			Destroy(gameObject);
		}
	}
}

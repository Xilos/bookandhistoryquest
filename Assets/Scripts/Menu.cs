﻿using UnityEngine;

public class Menu : MonoBehaviour
{
	[SerializeField]
	private GameObject _menuPopup;


	public void CloseApplication() => Application.Quit();

	public void PickHistoryMuseum()
	{
		ProgressWriter.PickHistoryMuseum();
		_menuPopup.SetActive(true);
	}

	public void PickBookMuseum()
	{
		ProgressWriter.PickBookMuseum();
		_menuPopup.SetActive(true);
	}

	public void StartNew() => ProgressWriter.StartNew();

	public void Continue() => ProgressWriter.Continue();

	public void Cancel() => _menuPopup.SetActive(false);
}

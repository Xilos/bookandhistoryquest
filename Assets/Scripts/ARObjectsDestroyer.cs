﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine;

public class ARObjectsDestroyer : MonoBehaviour
{
	private GameRay _gameRay;

	public List<GameObject> objects;


	private void Start()
	{
		_gameRay = GameRay.Instance;
	}

	private void Update()
	{
		if (objects.Any() == false)
		{
			ScenePicker.OpenNextSceneStatic();
		}

		if (_gameRay.HitObject == null)
			return;

		GameObject gameRayHitObject = _gameRay.HitObject;

		if (gameRayHitObject != null && objects.Contains(gameRayHitObject))
		{
			objects.Remove(gameRayHitObject);
			Destroy(gameRayHitObject);
		}
	}
}

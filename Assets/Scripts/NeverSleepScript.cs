﻿using UnityEngine;

public class NeverSleepScript : MonoBehaviour
{
	private void Awake() => Screen.sleepTimeout = SleepTimeout.NeverSleep;
}

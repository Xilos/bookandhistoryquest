﻿using UnityEngine;

public class BookMuseumUnlocker : MonoBehaviour
{
	private void Awake()
	{
		PlayerPrefs.SetInt("BookMuseumIsUnlocked", 1);
	}
}

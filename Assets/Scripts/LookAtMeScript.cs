﻿using System.Linq;

using UnityEngine;

public class LookAtMeScript : MonoBehaviour
{
	void Update()
	{
		Vector3 position = Camera.allCameras.First(x => x.isActiveAndEnabled).transform.position;

		transform.LookAt(new Vector3(position.x, 0, position.z));
	}
}

﻿using UnityEngine;
using UnityEngine.UI;


public class ButtonUnlocker : MonoBehaviour
{
	[SerializeField]
	private string _prefKey;

	[SerializeField]
	private int _unlockValue = 1;


	private void Awake()
	{
		var button = GetComponent<Button>();
		int prefValue = PlayerPrefs.GetInt(_prefKey);

		if (prefValue == _unlockValue)
		{
			button.interactable = true;
		}
	}
}

﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;


[RequireComponent(typeof(ARTrackedImageManager))]
public class TrackedImageInfoMultipleManager : MonoBehaviour
{
	private GameObject _arObject;

	[SerializeField]
	private GameObject arPrefab;

	private ARTrackedImageManager m_TrackedImageManager;

	private void Awake()
	{
		m_TrackedImageManager = GetComponent<ARTrackedImageManager>();

		GameObject newARObject = Instantiate(arPrefab, Vector3.zero, Quaternion.identity);
		newARObject.name = arPrefab.name;
		_arObject = newARObject;
		_arObject.SetActive(false);
	}

	private void OnEnable()
	{
		m_TrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
	}

	private void OnDisable()
	{
		m_TrackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
	}

	private void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
	{
		foreach (ARTrackedImage trackedImage in eventArgs.added)
		{
			if (trackedImage.referenceImage.name != "Map")
				continue;

			UpdateARImage(trackedImage);
		}

		foreach (ARTrackedImage trackedImage in eventArgs.updated)
		{
			if (trackedImage.referenceImage.name != "Map")
				continue;

			UpdateARImage(trackedImage);
			if (trackedImage.trackingState == TrackingState.None)
			{
				_arObject.SetActive(false);
			}
		}

		foreach (ARTrackedImage trackedImage in eventArgs.removed)
		{
			if (trackedImage.referenceImage.name != "Map")
				continue;

			_arObject.SetActive(false);
		}
	}

	private void UpdateARImage(ARTrackedImage trackedImage)
	{
		// Assign and Place Game Object
		AssignGameObject(trackedImage.transform.position, trackedImage.transform.rotation);

		//Debug.Log($"trackedImage.referenceImage.name: {trackedImage.referenceImage.name}");
	}

	private void AssignGameObject(Vector3 newPosition, Quaternion rotation)
	{
		if (arPrefab != null)
		{
			GameObject goARObject = _arObject;
			goARObject.SetActive(true);
			goARObject.transform.position = newPosition;
			goARObject.transform.rotation = rotation;
		}
	}
}
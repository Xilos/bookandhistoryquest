﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;


public class ProgressWriter : MonoBehaviour
{
	private static string _museumProgressKey = "book_progress";

	public List<string> notWritable;

	private void Awake()
	{
		SceneManager.sceneLoaded += SceneManager_OnsceneLoaded;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			SceneManager.LoadScene(0);
		}
	}

	private void SceneManager_OnsceneLoaded(Scene scene, LoadSceneMode mode)
	{
		string sceneName = scene.name;
		if (notWritable.Contains(sceneName))
			return;

		PlayerPrefs.SetString(_museumProgressKey, sceneName);
	}

	private void OnDestroy()
	{
		SceneManager.sceneLoaded -= SceneManager_OnsceneLoaded;
	}

	public static void PickHistoryMuseum() => _museumProgressKey = "history_progress";

	public static void PickBookMuseum() => _museumProgressKey = "book_progress";

	public static void Continue()
	{
		string sceneName = PlayerPrefs.GetString(_museumProgressKey);
		if (string.IsNullOrEmpty(sceneName))
		{
			StartNew();
			return;
		}

		SceneManager.LoadScene(sceneName);
	}

	public static void StartNew()
	{
		if (_museumProgressKey == "history_progress")
		{
			OpenFirstHistoryScene();
		}
		else
		{
			OpenFirstBookScene();
		}
	}


	private static void OpenFirstHistoryScene() => ScenePicker.OpenPrevSceneStatic();

	private static void OpenFirstBookScene()    => ScenePicker.OpenNextSceneStatic();
}

﻿#if UNITY_EDITOR

using System;
using System.ComponentModel;
using System.Reflection;

using UnityEditor;

[CustomEditor(typeof(ScenePicker), true)]
public class ScenePickerEditor : Editor
{
	public override void OnInspectorGUI()
	{
		var picker = target as ScenePicker;
		if (picker == null)
			return;

		ModifyStringFieldToSceneAsset(picker.prevScene, nameof(picker.prevScene));
		ModifyStringFieldToSceneAsset(picker.nextScene, nameof(picker.nextScene));
		ModifyStringFieldToSceneAsset(picker.helpScene, nameof(picker.helpScene));
	}

	private void ModifyStringFieldToSceneAsset(string field, string fieldName)
	{
		var oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(field);

		serializedObject.Update();

		EditorGUI.BeginChangeCheck();

		Type scenePickerType = typeof(ScenePicker);
		FieldInfo fiPrivate = scenePickerType.GetField(fieldName);
		string description  = ((DescriptionAttribute) fiPrivate.GetCustomAttribute(typeof(DescriptionAttribute))).Description;

		var newScene = EditorGUILayout.ObjectField(description, oldScene, typeof(SceneAsset), false) as SceneAsset;
		if (newScene == null)
			return;

		if (EditorGUI.EndChangeCheck())
		{
			string newPath = newScene.name;
			SerializedProperty scenePathProperty = serializedObject.FindProperty(fieldName);
			scenePathProperty.stringValue = newPath;
		}

		serializedObject.ApplyModifiedProperties();
	}
}

#endif
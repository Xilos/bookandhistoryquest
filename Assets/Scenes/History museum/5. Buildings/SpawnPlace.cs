﻿using System;

using UnityEngine;
using UnityEngine.UI;


public class SpawnPlace : MonoBehaviour
{
	private Sprite _initialSprite;

	[SerializeField] 
	private Sprite _collidedSprite;

	private Image _image;


	public bool HasTaken { get; private set; }


	public event EventHandler Taken;


	void Awake()
	{
		_image = GetComponent<Image>();
		_initialSprite = _image.sprite;
	}

	private void OnTriggerEnter(Collider other)
	{
		_image.sprite = _collidedSprite;
	}

	private void OnTriggerExit(Collider other)
	{
		_image.sprite = _initialSprite;
	}

	private void OnDestroy()
	{
		HasTaken = true;

		Taken?.Invoke(this, null);
	}
}

﻿using UnityEngine;
using UnityEngine.EventSystems;


public class Tower : MonoBehaviour, IDragHandler, IEndDragHandler
{
	private GameObject _currentTriggeredObject;

	private Vector3 _initialPosition;

	private Transform _towerLabel;

	[SerializeField]
	private GameObject _spawnPlace;


	private void Awake()
	{
		_initialPosition = transform.localPosition;
		_towerLabel = transform.GetChild(0);
	}


	public void OnDrag(PointerEventData eventData)
	{
		transform.position = eventData.position;
		_towerLabel.gameObject.SetActive(false);
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		bool hasBeenChosenWrongPlace = _currentTriggeredObject?.Equals(_spawnPlace) == false;
		bool hasNotBeenChosenPlace   = _currentTriggeredObject == null;

		if (hasNotBeenChosenPlace || hasBeenChosenWrongPlace)
		{
			if (hasBeenChosenWrongPlace)
			{
				AnimationScript.ShowWrongAnswerAnimation();
			}

			transform.localPosition = _initialPosition;
			_towerLabel.gameObject.SetActive(true);
			return;
		}

		transform.position = _spawnPlace.transform.position;
		Destroy(_spawnPlace);
		Destroy(this);
	}


	private void OnTriggerEnter(Collider other)
	{
		_currentTriggeredObject = other.gameObject;
	}

	private void OnTriggerExit(Collider other)
	{
		if (_currentTriggeredObject?.Equals(other.gameObject) == true)
			_currentTriggeredObject = null;
	}
}

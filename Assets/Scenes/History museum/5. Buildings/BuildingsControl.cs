﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

public class BuildingsControl : MonoBehaviour
{
	[SerializeField]
	private List<SpawnPlace> _places;

	[SerializeField]
	private GameObject _nextButton;


	private int EmptyPlacesCount => _places.Count(place => place.HasTaken == false);


	private void Awake()
	{
		_places.ForEach(place => place.Taken += Place_OnTaken);
	}

	private void Place_OnTaken(object sender, EventArgs e)
	{
		if (EmptyPlacesCount <= 0)
		{
			_nextButton.SetActive(true);
		}
	}
}

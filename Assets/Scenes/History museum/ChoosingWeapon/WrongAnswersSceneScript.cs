﻿using UnityEngine;
using UnityEngine.UI;


public class WrongAnswersSceneScript : MonoBehaviour
{
	[SerializeField]
	private Text _text;


	void Awake() => _text.text = $"{_text.text}{PlayerPrefs.GetString("WrongCheckedToggles")}";
}

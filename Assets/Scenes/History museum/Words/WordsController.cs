﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;


public class WordsController : MonoBehaviour
{
	public List<WordToggle> leftToggles;

	public List<Toggle> rightToggles;

	public GameObject nextButton;


	public void OnToggleValueChanged(bool value)
	{
		WordToggle leftToggle  = leftToggles  .FirstOrDefault(t => t.isOn);
		Toggle     rightToggle = rightToggles .FirstOrDefault(t => t.isOn);

		if (leftToggle == null || rightToggle == null)
			return;

		if (!leftToggle.Complementary.Equals(rightToggle))
		{
			//Запустить анимацию неправильного ответа

			leftToggle.isOn  = false;
			rightToggle.isOn = false;

			return;
		}

		leftToggles  .Remove(leftToggle);
		rightToggles .Remove(rightToggle);

		leftToggle  .gameObject.SetActive(false);
		rightToggle .gameObject.SetActive(false);

		if (leftToggles  .Any(t => t.gameObject.activeSelf) == false &&
		    rightToggles .Any(t => t.gameObject.activeSelf) == false)
		{
			nextButton.SetActive(true);
		}
	}
}

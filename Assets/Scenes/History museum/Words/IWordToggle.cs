﻿using UnityEngine.UI;


public interface IWordToggle
{
	Toggle Complementary { get; }
}

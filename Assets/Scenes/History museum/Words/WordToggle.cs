﻿using UnityEngine;
using UnityEngine.UI;


public class WordToggle : MonoBehaviour, IWordToggle
{
	[SerializeField]
	private Toggle _complementary;

	public Toggle Complementary => _complementary;

	public bool isOn
	{
		get => GetComponent<Toggle>()?.isOn ?? false;
		set
		{
			var toggle = GetComponent<Toggle>();
			if (toggle == null)
				return;

			toggle.isOn = value;
		}
	}
}

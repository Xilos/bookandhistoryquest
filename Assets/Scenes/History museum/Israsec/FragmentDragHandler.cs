﻿using System;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class FragmentDragHandler : MonoBehaviour, IDragHandler, IEndDragHandler
{
	private const float Tolerance = 30f;

	[SerializeField]
	private GameObject _fragmentPlace;

	private Vector2 _initialPosition;


	public event EventHandler FragmentDragged;


	void Start() => _initialPosition = transform.position;

	public void OnDrag(PointerEventData eventData)
	{
		transform.position = eventData.position;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		Vector3 fragmentPosition = _fragmentPlace.transform.position;

		if (Mathf.Abs(transform.position.x - fragmentPosition.x) <= Tolerance &&
		    Mathf.Abs(transform.position.y - fragmentPosition.y) <= Tolerance)
		{
			transform.position = new Vector2(fragmentPosition.x, fragmentPosition.y);

			MakeFragmentPlaceVisible();
			OnFragmentDragged();

			Destroy(gameObject);
		}
		else
		{
			transform.position = new Vector2(_initialPosition.x, _initialPosition.y);
		}
	}


	private void MakeFragmentPlaceVisible()
	{
		Color fragmentColor = _fragmentPlace.GetComponent<Image>().color;
		_fragmentPlace.GetComponent<Image>().color = new Color(fragmentColor.r, fragmentColor.g, fragmentColor.b, 0.7f);
	}

	private void OnFragmentDragged() => FragmentDragged?.Invoke(this, EventArgs.Empty);
}

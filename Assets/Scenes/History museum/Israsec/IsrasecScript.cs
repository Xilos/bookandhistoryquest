﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;


public class IsrasecScript : MonoBehaviour
{
	[SerializeField]
	private List<FragmentDragHandler> _fragments;

	[SerializeField]
	private List<GameObject> _places;

	[SerializeField]
	private GameObject _nextButton;


	void Start()
	{
		_fragments.ForEach(f => f.FragmentDragged += OnFragmentDragged);
	}


	private void OnFragmentDragged(object sender, EventArgs e)
	{
		_fragments.Remove((FragmentDragHandler) sender);
		if (_fragments.Any())
			return;

		MakeFragmentPlacesVisible();
		_nextButton.SetActive(true);
	}

	private void MakeFragmentPlacesVisible()
	{
		foreach (GameObject place in _places)
		{
			Color fragmentColor = place.GetComponent<Image>().color;
			place.GetComponent<Image>().color = new Color(fragmentColor.r, fragmentColor.g, fragmentColor.b, 1);
		}
	}
}

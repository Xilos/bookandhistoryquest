﻿using System.IO;

using UnityEditor;

using UnityEngine;


public class TakeScreenshotInEditor : ScriptableObject
{
	private const string FileName = "Screenshot ";
	private static int _startNumber = 1;

	[MenuItem("Custom/Take Screenshot of Game View %^s")]
	private static void TakeScreenshot()
	{
		int number = _startNumber;
		string name = "" + number;

		while (File.Exists(FileName + name + ".png"))
		{
			number++;
			name = "" + number;
		}

		_startNumber = number + 1;

		ScreenCapture.CaptureScreenshot("C:\\Screenshots\\" + FileName + name + ".png");
	}
}
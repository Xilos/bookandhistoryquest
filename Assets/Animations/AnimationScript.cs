﻿using UnityEngine;

public class AnimationScript : MonoBehaviour
{
	private static GameObject _animationGameObject;


	private void Awake()
	{
		_animationGameObject = gameObject;
	}


	public static void ShowWrongAnswerAnimation()
	{
		_animationGameObject.GetComponent<Animator>().Play("WrongAnswerAnimation");
	}
}
